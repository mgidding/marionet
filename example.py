import trainer
import player
from pathlib import Path
import datetime


save_dir = Path("checkpoints") / datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
checkpoint_location = Path("checkpoints/Trained/Session 1/mario_net_6.chkpt")

trainer.train(save_dir, checkpoint_location=checkpoint_location, checkpoint_interval=1e5)
player.play(checkpoint_location=checkpoint_location, loop=True)
