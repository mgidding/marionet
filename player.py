import torch.cuda

from network import MarioNet
import preprocessor


def play(checkpoint_location, loop):
    env = preprocessor.initialise(render_mode='human')
    env = preprocessor.preprocess(env)

    state_dim = (4, 84, 84)
    action_dim = env.action_space.n

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    checkpoint = torch.load(checkpoint_location)

    marionet = MarioNet(state_dim, action_dim).float()
    marionet.to(device=device)
    marionet.load_state_dict(checkpoint["model"])
    marionet.eval()

    state = env.reset()
    while True:
        # Run agent on the state
        state = state[0].__array__() if isinstance(state, tuple) else state.__array__()
        state = torch.tensor(state, device=device).unsqueeze(0)
        with torch.no_grad():
            action_values = marionet(state, model="online")
            action = torch.argmax(action_values, axis=1).item()

        # Agent performs action
        next_state, reward, done, trunc, info = env.step(action)

        # Update state
        state = next_state
        import time
        time.sleep(0.066)

        # Check if end of game
        if done or info["flag_get"]:
            if not loop:
                break
            state = env.reset()
