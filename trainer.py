import preprocessor
from agent import Mario
from logger import MetricLogger


def training_loop(env, mario, logger):
    state = env.reset()

    while True:
        # Run agent on the state
        action = mario.act(state)

        # Agent performs action
        next_state, reward, done, trunc, info = env.step(action)

        # Remember
        mario.cache(state, next_state, action, reward, done)

        # Learn
        q, loss = mario.learn()

        # Logging
        logger.log_step(reward, loss, q)

        # Update state
        state = next_state

        # Check if end of game
        if done or info["flag_get"]:
            break
    logger.log_episode()


def train(save_dir, episodes=40001, checkpoint_location=None, watch=False, checkpoint_interval=5e5):
    save_dir.mkdir(parents=True)

    render_mode = 'human' if watch else 'rgb'
    env = preprocessor.initialise(render_mode=render_mode)
    env = preprocessor.preprocess(env)

    mario = Mario(state_dim=(4, 84, 84), action_dim=env.action_space.n, save_dir=save_dir, save_every=checkpoint_interval)
    if checkpoint_location is not None:
        mario.load(checkpoint_location)
    logger = MetricLogger(save_dir)

    episodes = episodes
    for e in range(episodes):
        training_loop(env, mario, logger)
        if e % 20 == 0:
            logger.record(episode=e, epsilon=mario.exploration_rate, step=mario.curr_step)
